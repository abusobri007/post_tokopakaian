<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model {

    public function getData_Karyawan()
    {
        $query = $this->db->query("SELECT * FROM karyawan");
        return $query->result_array();
    }

    public function getData_KaryawanRow($id)
    {
        $query = $this->db->query("SELECT * FROM karyawan WHERE id = $id");
        return $query->row_array();
    }

}
