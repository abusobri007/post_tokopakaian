<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategory_model extends CI_Model {

    public function getData_Kategory()
    {
        $query = $this->db->query("SELECT * FROM kategory");
        return $query->result_array();
    }

    public function getData_KategoryRow($id)
    {
        $query = $this->db->query("SELECT * FROM Kategory WHERE id = $id");
        return $query->row_array();
    }

}
