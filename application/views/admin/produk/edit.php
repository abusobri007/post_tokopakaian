
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Data</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="card card-primary card-outline">
              <div class="card-body">
                <form method="POST" action="<?=base_url('admin/produk/proses_edit/').$query['id']?>">
								<div class="mb-3">
                    <label class="form-label">ID</label>
                    <input type="text" name="id" class="form-control" value="<?=$query['id']?>">
                  </div>
									<div class="mb-3">
                    <label class="form-label">ID Kategory</label>
                    <input type="text" name="kategory_id" class="form-control" value="<?=$query['kategory_id']?>">
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Nama Produk</label>
                    <input type="text" name="nama_produk" class="form-control" value="<?=$query['nama_produk']?>">
                  </div>
									<div class="mb-3">
                    <label class="form-label">Jenis Produk</label>
                    <input type="text" name="jenis_produk" class="form-control" value="<?=$query['jenis_produk']?>">
                  </div>
									<div class="mb-3">
                    <label class="form-label">Harga</label>
                    <input type="number" name="harga" class="form-control" value="<?=$query['harga']?>">
                  </div>
									<div class="mb-3">
                    <label class="form-label">Stok</label>
                    <input type="number" name="stok" class="form-control" value="<?=$query['stok']?>">
                  </div>
                  <button type="submit" class="btn btn-primary">Edit</button>
                </form>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  $(function () {
    $("#contoh").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>

  
