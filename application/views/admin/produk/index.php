
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <div class="m-0">
                  <a href="<?=base_url('admin/produk/tambah')?>">
                    <button class="btn btn-primary">Tambah Data</button>
                  </a>
                </div>
              </div>
              <div class="card-body">
                <table id="contoh" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
										<th>Kategory Id</th>
                    <th>Nama Produk</th>
                    <th>Jenis Produk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=1;
                    foreach ($query as $key => $value) {
                    ?>
                    <tr>
										 <td><?=$value['id']?></td>
											<td><?=$value['kategory_id']?></td>
                      <td><?=$value['nama_produk']?></td>
                      <td><?=$value['jenis_produk']?></td>
                      <td><?=$value['harga']?></td>
                      <td><?=$value['stok']?></td>
                     
                      <td>
                        <a href="<?=base_url('admin/produk/edit/').$value['id']?>">
                          <button class="btn btn-success">Edit</button>
                        </a>
                        <a href="<?=base_url('admin/produk/proses_hapus/').$value['id']?>">
                          <button class="btn btn-danger">Delete</button>
                        </a>
                      </td>
                    </tr>
                    <?php
                    $i++;
                    }
                    ?>                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  $(function () {
    $("#contoh").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>

  
