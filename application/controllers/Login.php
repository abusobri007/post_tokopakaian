<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model'); // load model auth_model
    }

	public function index()
	{
		// cek apakah user sudah login atau belum
        if (!$this->session->userdata('id_admin')) {
            // validasi form login
	        $this->form_validation->set_rules('email', 'Email', 'required');
	        $this->form_validation->set_rules('password', 'Password', 'required');

	        if ($this->form_validation->run() == false) {
	            // tampilkan halaman login jika validasi gagal
	            $this->load->view('login');
	        } else {
	            // cek login
	            $this->proses_login();
	        }
        }
        else{
        	redirect('admin/dashboard'); // redirect ke halaman dashboard
        }
	}

	public function proses_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$user = $this->login_model->getData_Admin($email);
		if ($user) {
            // cek apakah password cocok
            	if (password_verify($password, $user['password'])) {
	                $data = [
	                    'id_admin'			=> $user['id']
	                ];
                	$this->session->set_userdata($data);
                	//printf($data['id_prodiSMS']);
                	redirect('admin/dashboard'); // redirect ke halaman dashboard
	            } 
	            else {
	                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong password!</div>');
	                redirect('login'); // redirect kembali ke halaman login
	            }
	        } 
	        else {
	            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username not found!</div>');
	            redirect('login'); // redirect kembali ke halaman login
	        }
	}

	public function logout()
    {
        $this->session->unset_userdata('id_admin');
        redirect('login'); // redirect kembali ke halaman login
    }
}
