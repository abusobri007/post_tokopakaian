<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url'); // load helper url
		$this->load->model('karyawan_model');
        if (!$this->session->userdata('id_admin')) {
            redirect('login'); // redirect ke halaman login jika belum login
        }
    }
	
	public function index()
	{
		$query = $this->karyawan_model->getData_Karyawan();
		$data['query'] = $query;
		$this->loadAdmin('karyawan/index',$data);
	}

	public function tambah(){
		$data[''] = "";
		$this->loadAdmin('karyawan/tambah',$data);
	}

	public function proses_tambah(){
		$data = array(
        'nama_karyawan' => $this->input->post('nama_karyawan'),
        'alamat' => $this->input->post('alamat'),
        'nomer_telpon' => $this->input->post('nomer_telpon'),
		);
		$this->db->insert('karyawan', $data);
		redirect('admin/karyawan');
	}

	public function edit($id){
		$query = $this->karyawan_model->getData_KaryawanRow($id);
		$data['query'] = $query;
		$this->loadAdmin('karyawan/edit',$data);
	}

	public function proses_edit($id){
		$data = array(
        'nama_karyawan' => $this->input->post('nama_karyawan'),
        'alamat' => $this->input->post('alamat'),
        'nomer_telpon' => $this->input->post('nomer_telpon'),
		);
		$this->db->where('id', $id);
		$this->db->update('karyawan', $data);
		redirect('admin/karyawan');
	}

	public function proses_hapus($id){
		$this->db->where('id', $id);
		$this->db->delete('karyawan');
		redirect('admin/karyawan');
	}
}
