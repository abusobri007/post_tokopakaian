<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url'); // load helper url
        $this->load->model('produk_model');
        if (!$this->session->userdata('id_admin')) {
            redirect('login'); // redirect ke halaman login jika belum login
        }
    }
	
	public function index()
	{
		$query = $this->produk_model->getData_Produk();
		$data['query'] = $query;
		$this->loadAdmin('produk/index',$data);
	}

	public function tambah(){
		$data[''] = "";
		$this->loadAdmin('produk/tambah',$data);
	}

	public function proses_tambah(){
		$data = array(
        'id' => $this->input->post('id'),
        'kategory_id' => $this->input->post('kategory_id'),
        'nama_produk' => $this->input->post('nama_produk'),
        'jenis_produk' => $this->input->post('jenis_produk'),
        'harga' => $this->input->post('harga'),
        'stok' => $this->input->post('stok'),
        'deskripsi' => $this->input->post('deskripsi'),
		);
		$this->db->insert('contoh', $data);
		redirect('admin/produk');
	}

	public function edit($id){
		$query = $this->produk_model->getData_ProdukRow($id);
		$data['query'] = $query;
		$this->loadAdmin('produk/edit',$data);
	}

	public function proses_edit($id){
		$data = array(
        'id' => $this->input->post('id'),
        'kategory_id' => $this->input->post('kategory_id'),
        'nama_produk' => $this->input->post('nama_produk'),
        'jenis_produk' => $this->input->post('jenis_produk'),
        'harga' => $this->input->post('harga'),
        'deskripsi' => $this->input->post('deskirpsi'),
        'stok' => $this->input->post('stok'),
		);
		$this->db->where('id', $id);
		$this->db->update('contoh', $data);
		redirect('admin/produk');
	}

	public function proses_hapus($id){
		$this->db->where('id', $id);
		$this->db->delete('contoh');
		redirect('admin/produk');
	}
}
