<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategory extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url'); // load helper url
		$this->load->model('kategory_model');
        if (!$this->session->userdata('id_admin')) {
            redirect('login'); // redirect ke halaman login jika belum login
        }
    }
	
	public function index()
	{
		$query = $this->kategory_model->getData_Kategory();
		$data['query'] = $query;
		$this->loadAdmin('kategory/index',$data);
	}


	public function tambah(){
		$data[''] = "";
		$this->loadAdmin('kategory/tambah',$data);
	}

	public function proses_tambah(){
		$data = array(
	               'id' => $this->input->post('id'),
                               'nama_kategory' => $this->input->post('nama_kategory'),
                               'deskripsi' => $this->input->post('deskripsi'),
                              
		);
		$this->db->insert('kategory', $data);
		redirect('admin/kategory');
	}

	public function edit($id){
		$query = $this->kategory_model->getData_KategoryRow($id);
		$data['query'] = $query;
		$this->loadAdmin('kategory/edit',$data);
	}

	public function proses_edit($id){
		$data = array(
		'id' => $this->input->post('id'),
        		'nama_kategory' => $this->input->post('nama_kategory'),
       		'deskripsi' => $this->input->post('deskripsi'),
		);
		$this->db->where('id', $id);
		$this->db->update('kategory', $data);
		redirect('admin/kategory');
	}

	public function proses_hapus($id){
		$this->db->where('id', $id);
		$this->db->delete('kategory');
		redirect('admin/kategory');
	}
}
